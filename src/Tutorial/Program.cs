﻿using System;
using Tutorial.IO;
using Tutorial.Git;
using Tutorial.Linq;
using Tutorial.MySql;
using Tutorial.Reflection;
using Tutorial.S3;
using Tutorial.Crypt;
using Tutorial.Algorithm;
using Tutorial.Collection;
using Tutorial.Date;

namespace Tutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                RunIOTest();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.ToString());
            }
        }

        static void RunLinqTest()
        {
            LinqSelectTest.Run();
        }

        static void RunDataTimeTest()
        {
            DateTimeTest.Run();
        }

        static void RunEnumerableTest()
        {
            EnumerableTest.Run();
        }

        static void RunListTest()
        {
            ListTest.Run();
        }

        static void RunMergeSortTest()
        {
            MergeSortTest.Run();
        }

        static void RunCryptTest()
        {
            CryptTest.Run();
        }

        static void RunReflectionTest()
        {
            AssemblyTest.Run();
            ModuleTest.Run();
        }

        static void RunXmlTest()
        {
            XmlTest.Run();
        }

        static void RunMySqlTest()
        {
            MySqlTest.Run();
        }

        static void RunIOTest()
        {
            IOTest.Run();
        }

        static void RunGitTest()
        {
            RepositoryTest.Run();
            CloneTest.Run();
            RemoteTest.Run();
            FetchTest.Run();
            PullTest.Run();
            BranchTest.Run();
            CommitTest.Run();
            ResetTest.Run();
            LogTest.Run();
            LsFilesTest.Run();
            TagTest.Run();
            StatusTest.Run();
            PushTest.Run();
        }

        static void RunS3Test()
        {
            S3Test.Run();
        }
    }
}
