﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Tutorial.S3
{
    public class S3UploadInfo
    {
        public string Key { get; set; }
        public string FilePath { get; set; }
        public IDictionary<string, string> TagSet { get; set; }
        public IDictionary<string, string> Metadata { get; set; }
    }
}
