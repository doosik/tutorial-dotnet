﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace Tutorial.S3
{
    public class S3Client : IDisposable
    {
        private readonly string _accessKeyId = "AKIAX7EPYOWJPBNETPGX";
        private readonly string _secretAccessKey = "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";
        private readonly RegionEndpoint _bucketRegion = RegionEndpoint.APNortheast2;
        private readonly string _bucketName = "test-ecapm";
        private readonly long _partSize = 1024L * 1024L * 1024L * 5L;
        private IAmazonS3 _s3Client;

        public S3Client()
        {
            _s3Client = new AmazonS3Client(_accessKeyId, _secretAccessKey, _bucketRegion);
        }

        public void Dispose()
        {
            _s3Client.Dispose();
        }

        public Task UploadAsync(S3UploadInfo uploadInfo, CancellationToken cancellationToken = default)
        {
            if (!File.Exists(uploadInfo.FilePath))
            {
                throw new FileNotFoundException("File not found", uploadInfo.FilePath);
            }

            var fileInfo = new FileInfo(uploadInfo.FilePath);
            if (fileInfo.Length >= _partSize)
            {
                throw new ArgumentOutOfRangeException("Maximum upload size exceeded");
            }

            var transferUtility = new TransferUtility(_s3Client);
            var transferUtilityRequest = new TransferUtilityUploadRequest
            {
                BucketName = _bucketName,
                Key = uploadInfo.Key,
                FilePath = uploadInfo.FilePath,
                PartSize = _partSize,
                StorageClass = S3StorageClass.Standard,
                //CannedACL = S3CannedACL.PublicRead, // Access Denied
            };

            if (uploadInfo.Metadata != null && uploadInfo.Metadata.Count > 0)
            {
                foreach (var kv in uploadInfo.Metadata)
                {
                    transferUtilityRequest.Metadata.Add(kv.Key, kv.Value);
                }
            }

            if (uploadInfo.TagSet != null && uploadInfo.TagSet.Count > 0)
            {
                var tagSet = new List<Tag>();
                foreach (var kv in uploadInfo.TagSet)
                {
                    tagSet.Add(new Tag { Key = kv.Key, Value = kv.Value });
                }
                transferUtilityRequest.TagSet = tagSet;
            }

            return transferUtility.UploadAsync(transferUtilityRequest, cancellationToken);
        }

        public Task DownloadAsync(S3DownloadInfo downloadInfo, CancellationToken cancellationToken = default)
        {
            var transferUtility = new TransferUtility(_s3Client);
            var transferUtilityRequest = new TransferUtilityDownloadRequest
            {
                BucketName = _bucketName,
                Key = downloadInfo.Key,
                FilePath = downloadInfo.FilePath
            };

            return transferUtility.DownloadAsync(transferUtilityRequest, cancellationToken);
        }

        public async Task<List<S3ObjectInfo>> GetObjectInfosAsync(string prefix, int maxCount, CancellationToken cancellationToken = default)
        {
            ListObjectsV2Request request = new ListObjectsV2Request
            {
                BucketName = _bucketName,
                Prefix = prefix,
                MaxKeys = maxCount
            };

            ListObjectsV2Response response;
            List<S3ObjectInfo> infos= new List<S3ObjectInfo>();

            do
            {
                response = await _s3Client.ListObjectsV2Async(request, cancellationToken);
                response.S3Objects.ForEach(item =>
                {
                    infos.Add(new S3ObjectInfo
                    {
                        Key = item.Key,
                        LastModified = item.LastModified,
                        Size = item.Size
                    });
                });

                if (infos.Count >= maxCount) break;
                request.ContinuationToken = response.NextContinuationToken;
            } while (response.IsTruncated);

            return infos;
        }
   }
}
