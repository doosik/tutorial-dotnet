﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Tutorial.S3
{
    public class S3ObjectInfo
    {
        public string Key { get; set; }
        public DateTime LastModified { get; set; }
        public long Size { get; set; }
    }
}
