﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Tutorial.Extensions;

namespace Tutorial.S3
{
    class S3Test : IDisposable
    {
        public static void Run()
        {
            Console.WriteLine("[ S3Test ]");

            using (var test = new S3Test())
            {
                test.UploadFile();
            }
        }

        public void Dispose()
        {
        }

        public void UploadFile()
        {
            Console.WriteLine("- UploadFile");

            var type = "Error";
            var filePath = @"D:\Projects\ec-apm\Temp\APM\UPLOADING\DETAIL\20190807151724.c648e2be6a8a40bea3dbe0d38476b4b3.DETAIL";

            var now = DateTime.Now;
            var day = now.ToString("yyyy-MM-dd");
            var hour = now.ToString("HH");
            var minute = (now.Minute / 10) * 10;
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var key = String.Format("{0}/{1}/{2}/{3:D2}/{4}", type, day, hour, minute, fileName);

            var uploadInfo = new S3UploadInfo
            {
                Key = key,
                FilePath = filePath,
                TagSet = new Dictionary<string, string>()
                {
                    { "type", type },
                    { "day", day }
                }
            };

            using (var s3Client = new S3Client())
            {
                s3Client.UploadAsync(uploadInfo).Wait();
            }
        }
    }
}
