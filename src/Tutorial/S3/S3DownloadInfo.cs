﻿using System;
using System.IO;

namespace Tutorial.S3
{
    public class S3DownloadInfo
    {
        public string Key { get; set; }
        public string FilePath { get; set; }
    }
}
