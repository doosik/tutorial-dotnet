﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial.RegularExpressions
{
    class RegexTest
    {
        /*
        public static void Run()
        {
            Console.WriteLine("[ RegexTest ]");

            var test = new RegexTest();
            test.PrintTime();
        }

        public static void DuplicateWordGroup()
        {
            Console.WriteLine("- PrintTime");

            string pattern = @"(\w+)\s(\1)";
            string input = "He said that that was the the correct answer.";

            foreach (Match match in Regex.Matches(input, pattern, RegexOptions.IgnoreCase))
                Console.WriteLine("Duplicate '{0}' found at positions {1} and {2}.",
                                  match.Groups[1].Value, match.Groups[1].Index, match.Groups[2].Index);
        }

        //
        // https://docs.microsoft.com/ko-kr/visualstudio/ide/using-regular-expressions-in-visual-studio?view=vs-2019
        // https://docs.microsoft.com/ko-kr/dotnet/standard/base-types/grouping-constructs-in-regular-expressions
        //
        // Find : map.Add\("\/ECERP(\/Contents\/.+)"\)
        // Replace : map.Add(Optimizer.Url("~$1"))
        //
        // compress-archive test.txt test.zip
        // expand-archive -path 'c:\users\john\desktop\iislogs.zip' -destinationpath '.\unzipped'
        //

        protected HttpActionResult ExecuteCore(IERPSessionContext context)
        {
            RegisterScriptPath(map => {
                map.Add(Optimizer.Url("~/Contents/js/widget.crop.js"));
                map.Add(Optimizer.Url("~/Popup.Common/Views/CM308P.js"));
            });
            RegisterScript(sb => sb.Append("require('pluploader');"));
            RegisterScriptPath(map => {
                map.Add("/ECErp/Contents/js/lib/jquery.imgareaselect.pack.js");
            });
            RegisterViewClassName("mainPage", "page-fluid hidden");

            return View("DefaultViewPage", this);
        }
        */
    }
}
