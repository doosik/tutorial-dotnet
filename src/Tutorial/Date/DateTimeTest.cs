﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial.Date
{
    class DateTimeTest
    {
        public static void Run()
        {
            Console.WriteLine("[ DateTimeTest ]");

            var test = new DateTimeTest();
            test.PrintFormatTime();
            test.ConvertStringToDataTime();
            test.ConvertUnixTime();
        }

        private void PrintFormatTime()
        {
            Console.WriteLine("- PrintFormatTime");

            var dtString = "2019-08-28 13:38:54.088111";
            DateTime dt = DateTime.Parse(dtString);

            Console.WriteLine("string: {0}, date-time: {1}", dtString, dt.ToString("yyyyMMddHHmm"));
        }

        private void ConvertStringToDataTime()
        {
            Console.WriteLine("- ConvertStringToDataTime");

            var dtString = "2019-08-28 13:38:54.088111";
            DateTime dt = DateTime.Parse(dtString);
            Console.WriteLine("string: {0}, date-time: {1}", dtString, dt.ToString("yyyy-MM-dd HH:mm:ss.fffffff"));
            Console.WriteLine("string: {0}, date-time: {1}", dtString, dt.ToString("o"));
        }

        private void ConvertUnixTime()
        {
            Console.WriteLine("- ConvertUnixTime");

            var dtString = "2019-09-25 10:18:46.993304";
            var dateTime = DateTime.Parse(dtString);

            var unixTime = dateTime.ToUnixTime();
            Console.WriteLine("dt: {0}, unix-time: {1}", dateTime.ToString("o"), unixTime);

            dateTime = unixTime.ToLocalTime();
            Console.WriteLine("dt: {0}, unix-time: {1}", dateTime.ToString("o"), unixTime);
        }
    }
}
