﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tutorial.Date
{
    public static class DateTimeExtensions
    {
        /*
        DateTime d = DateTime.Parse("2019-08-28 13:38:54.088111");
        Console.WriteLine(d.ToString());
        TimeSpan s = TimeSpan.FromSeconds(0.3535353);
        Console.WriteLine(s.ToString());
        //ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz");
        */

        //
        // microseconds (1/1,000,000 second)
        // 1569374326 993304
        //
        // https://www.epochconverter.com/
        // https://www.timecalculator.net/milliseconds-to-date
        //

        // UnixTimeToDateTime
        public static DateTime ToLocalTime(this long unixTime)
        {
            // Milliseconds to Microseconds Conversion (ms to µs)
            if (unixTime < 10000000000000) unixTime *= 1000;

            var unixTimeStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            long unixTimeInTicks = (long)(((double)unixTime / 1000000) * TimeSpan.TicksPerSecond);
            return new DateTime(unixTimeStart.Ticks + unixTimeInTicks, DateTimeKind.Utc).ToLocalTime();
        }

        // DateTimeToUnixTime
        public static long ToUnixTime(this DateTime dateTime)
        {
            var unixTimeStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            long unixTimeInTicks = (dateTime.ToUniversalTime() - unixTimeStart).Ticks;
            return (long)(((double)unixTimeInTicks / TimeSpan.TicksPerSecond) * 1000000);
        }
    }
}
