﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial.IO
{
    class IOTest
    {
        private string basePath = @"D:\Temp";
        private string filePath = @"D:\Temp\xxx\test.txt";

        private string dir = @"D:\Temp\apm-backupx";
        private string path = @"D:\Temp\xxx\test.txt";

        public static void Run()
        {
            Console.WriteLine("[ IOTest ]");

            var test = new IOTest();
            test.PrintTime();
            test.GetFileName();
            test.GetFiles();
            test.TrimBasePath();
            test.GetDirectoryName();
            test.GetParent();
            test.GetDirectories_Directory();
            test.GetDirectories_DirectoryInfo();
            //test.CreateDirectory();
        }

        private void PrintTime()
        {
            Console.WriteLine("- PrintTime");

            var test = "/contents//xxx";
            Console.WriteLine("- xxx : {0}", test.Replace("//", "/"));

            var fileInfo = new FileInfo(@"E:\Temp\hosts.txt");

            Console.WriteLine("path: {0}", Path.Combine(@"Contents\js\require", Path.GetFileNameWithoutExtension(fileInfo.Name), ".js"));
            Console.WriteLine("time: {0}", fileInfo.LastWriteTimeUtc.ToString());
            Console.WriteLine("time: {0}", fileInfo.LastWriteTimeUtc.ToString("o"));
        }

        private void GetFileName()
        {
            Console.WriteLine("- GetFileName");

            var path = "~/a/b/c/d.txt";
            var dirName = Path.GetDirectoryName(path);
            var fileName = Path.GetFileName(path);
            var extension = Path.GetExtension(path);

            Console.WriteLine("path: {0}, dir: {1}, file: {2}, ext: {3}", path, dirName, fileName, extension);
        }

        private void GetFiles()
        {
            Console.WriteLine("- GetFiles");

            var baseInfo = new DirectoryInfo(basePath);
            var fileInfos = baseInfo.GetFiles("*.*", SearchOption.AllDirectories);
            foreach (var fileInfo in fileInfos)
            {
                var parentInfo = fileInfo.Directory;
                if (baseInfo.FullName.Equals(parentInfo.FullName, StringComparison.OrdinalIgnoreCase))
                    Console.WriteLine("path: {0}, parent: {1}", fileInfo.FullName, parentInfo.FullName);
            }
        }

        private void TrimBasePath()
        {
            Console.WriteLine("- TrimBasePath");

            var relativePath = filePath.Substring(basePath.Length);

            Console.WriteLine("path: {0}, trim base: {1}", filePath, relativePath);
        }

        private void GetDirectoryName()
        {
            Console.WriteLine("- GetDirectoryNameForFile");

            var dirName = Path.GetDirectoryName(path);
            Console.WriteLine("path: {0}, Path.GetDirectoryName: {1}", path, dirName);

            var dirInfo = new DirectoryInfo(dir);
            Console.WriteLine("path: {0}, DirectoryInfo.Name: {1}", dir, dirInfo.Name);
        }

        private void GetParent()
        {
            Console.WriteLine("- GetParent");

            DirectoryInfo dirInfo = Directory.GetParent(path);
            Console.WriteLine("path: {0}, parent name: {1}", path, dirInfo.Name);
        }

        private void GetDirectories_Directory()
        {
            Console.WriteLine("- GetDirectories : Directory.GetDirectories");

            var dirs = Directory.GetDirectories(basePath);
            foreach (var dir in dirs)
            {
                var dirInfo = new DirectoryInfo(dir);
                var files = dirInfo.GetFiles("*.tgz");
                Console.WriteLine("base path: {0}, dir: {1}, files: {2}", basePath, dirInfo.Name, files.Length);
            }
        }

        private void GetDirectories_DirectoryInfo()
        {
            Console.WriteLine("- GetDirectories : DirectoryInfo");

            var baseDirInfo = new DirectoryInfo(basePath);
            var dirInfos = baseDirInfo.GetDirectories();
            foreach (var dirInfo in dirInfos)
            {
                var files = dirInfo.GetFiles("*.tgz");
                Console.WriteLine("base path: {0}, dir: {1}, files: {2}", basePath, dirInfo.Name, files.Length);
            }
        }

        /*
        private void CreateDirectory()
        {
            Console.WriteLine("- CreateDirectory");

            DirectoryInfo dirInfo = Directory.CreateDirectory(Path.GetDirectoryName(path));
            Console.WriteLine("path: {0}, parent name: {1}", path, dirInfo.Name);
        }
        */
    }
}
