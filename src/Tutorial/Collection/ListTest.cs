﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tutorial.Extensions;

namespace Tutorial.Collection
{
    public class ListTest
    {
        class Person
        {
            public string Name { get; set; }
            public List<Person> Children { get; set; }

            public void Loop(Action<Person> action)
            {
                action(this);
                foreach (var c in Children)
                {
                    c.Loop(action);
                }
            }
        }

        public static void Run()
        {
            Console.WriteLine("[ ListTest ]");

            var test = new ListTest();

            test.PrintPerson();
            /*
            test.CreateAndIterate();
            test.CreateByInitializer();
            test.RemoveElementByObject();
            test.RemoveElementByIndex();
            */
        }

        public void PrintPerson()
        {
            DateTime d = DateTime.Parse("2019-08-28 13:38:54.088111");
            Console.WriteLine(d.ToString());
            TimeSpan s = TimeSpan.FromSeconds(0.3535353);
            Console.WriteLine(s.ToString());

            var root_c1_cs = new List<Person>
            {
                new Person { Name = "root_c1_c1", Children = new List<Person>() }
            };
            var root_cs = new List<Person>
            {
                new Person { Name = "root_c1", Children = new List<Person>() },
                new Person { Name = "root_c2", Children = root_c1_cs }
            };

            var root = new Person { Name = "root", Children = root_cs };

            root.Loop(p => Console.WriteLine(p.Name));
        }

        public void CreateAndIterate()
        {
            Console.WriteLine("- CreateAndIterate");

            // Create a list of strings.
            var salmons = new List<string>();
            salmons.Add("chinook");
            salmons.Add("coho");
            salmons.Add("pink");
            salmons.Add("sockeye");

            // Iterate through the list.
            foreach (var salmon in salmons)
            {
                Console.Write(salmon + " ");
            }
        }

        public void CreateByInitializer()
        {
            Console.WriteLine("- CreateByInitializer");

            // Create a list of strings by using a collection initializer.
            var salmons = new List<string> { "chinook", "coho", "pink", "sockeye" };

            for (var index = 0; index < salmons.Count; index++)
            {
                Console.Write(salmons[index] + " ");
            }
        }

        public void RemoveElementByObject()
        {
            Console.WriteLine("- RemoveElementByObject");

            var salmons = new List<string> { "chinook", "coho", "pink", "sockeye" };

            // Remove an element from the list by specifying the object.
            salmons.Remove("coho");

            // Iterate through the list.
            foreach (var salmon in salmons)
            {
                Console.Write(salmon + " ");
            }
        }

        public void RemoveElementByIndex()
        {
            Console.WriteLine("- RemoveElementByIndex");

            var numbers = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            // Remove odd numbers.
            for (var index = numbers.Count - 1; index >= 0; index--)
            {
                if (numbers[index] % 2 == 1)
                {
                    // Remove the element by specifying
                    // the zero-based index in the list.
                    numbers.RemoveAt(index);
                }
            }

            // Iterate through the list.
            numbers.ForEach(number => Console.Write(number + " "));
        }


    }
}
