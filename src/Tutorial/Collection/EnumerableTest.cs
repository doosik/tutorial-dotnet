﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tutorial.Extensions;

namespace Tutorial.Collection
{
    public class EnumerableTest
    {
        public static void Run()
        {
            Console.WriteLine("[ EnumerableTest ]");

            var test = new EnumerableTest();

            test.RunMultiTasks();
        }

        public void RunMultiTasks()
        {
            Console.WriteLine("- RunMultiTasks");

            var salmons = GetSalmons();
            var tasks = new List<Task>();

            tasks.Add(Task.Run(() =>
            {
                var name = Thread.CurrentThread.ManagedThreadId;

                // Iterate through the list.
                foreach (var salmon in salmons)
                {
                    Thread.Sleep(20);
                    Console.WriteLine($"[{name}]{salmon}");
                }
            }));

            tasks.Add(Task.Run(() =>
            {
                var name = Thread.CurrentThread.ManagedThreadId;

                // Iterate through the list.
                foreach (var salmon in salmons)
                {
                    Thread.Sleep(50);
                    Console.WriteLine($"[{name}]{salmon}");
                }
            }));

            Task.WaitAll(tasks.ToArray());
        }

        private IEnumerable<string> GetSalmons()
        {
            var salmons = new List<string> { "chinook", "coho", "pink", "sockeye" };

            for (var i = 0; i < salmons.Count; ++i)
            {
                yield return $"[{i}] {salmons[i]}";
            }
        }
    }
}
