﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tutorial.Extensions;

namespace Tutorial.Algorithm
{
    class MergeSortTest
    {
        public static void Run()
        {
            Console.WriteLine("[ MergeSortTest ]");

            var test = new MergeSortTest();
            test.SortIntArreay();
        }

        public void SortIntArreay()
        {
            Console.WriteLine("- SortIntArray");

            int[] srcArray = { 1, 98, 645, 64, 556, 345, 100, 234, 5, 5, 3, 7, 8, 7, 5 };
            int[] sortedArray;

            sortedArray = srcArray.MergeSort();
            sortedArray.Print();
        }
    }
}
