﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Tutorial.Linq
{
    class XmlTest
    {
        public static void Run()
        {
            var test = new XmlTest();
            test.LoadByXElement();
            test.LoadByXDocument();
            test.CreateXmlTree();
        }

        private void LoadByXElement()
        {
            // Create a simple document and write it to a file  
            File.WriteAllText("Test.xml",
                @"<Root>  
                    <Child1>1</Child1>  
                    <Child2>2</Child2>  
                    <Child3>3</Child3>  
                </Root>"
            );

            Console.WriteLine("Querying tree loaded with XElement.Load");
            Console.WriteLine("----");

            XElement doc = XElement.Load("Test.xml");
            IEnumerable<XElement> childList =
                from el in doc.Elements()
                select el;

            foreach (XElement e in childList)
            {
                Console.WriteLine("...");
                Console.WriteLine(e);
            }
            Console.WriteLine("");
        }

        private void LoadByXDocument()
        {
            // Create a simple document and write it to a file  
            File.WriteAllText("Test.xml",
                @"<Root>  
                    <Child1>1</Child1>  
                    <Child2>2</Child2>  
                    <Child3>3</Child3>  
                </Root>"
            );

            Console.WriteLine("Querying tree loaded with XDocument.Load");
            Console.WriteLine("----");

            XDocument doc = XDocument.Load("Test.xml");
            IEnumerable<XElement> childList =
                from el in doc.Elements()
                // from el in doc.Root.Elements()
                select el;

            foreach (XElement e in childList)
            {
                Console.WriteLine("...");
                Console.WriteLine(e);
            }
            Console.WriteLine("");
        }

        private void CreateXmlTree()
        {
            XElement contacts =
                new XElement("Contacts",
                    new XElement("Contact",
                        new XElement("Name", "Patrick Hines"),
                        new XElement("Phone", "206-555-0144",
                            new XAttribute("Type", "Home")),
                        new XElement("phone", "425-555-0145",
                            new XAttribute("Type", "Work")),
                        new XElement("Address",
                            new XElement("Street1", "123 Main St"),
                            new XElement("City", "Mercer Island"),
                            new XElement("State", "WA"),
                            new XElement("Postal", "68042")
                        )
                    )
                );

            Console.WriteLine("Xml Tree");
            Console.WriteLine("---");
            Console.WriteLine(contacts);
            Console.WriteLine("");
        }

        private void Outline()
        {
            File.WriteAllText("Test.xml",
                @"
                <?xml version=""1.0""?>  
                <PurchaseOrder PurchaseOrderNumber=\""99503\"" OrderDate=\""1999-10-20\"">  
                  <Address Type=\""Shipping\"">  
                    <Name>Ellen Adams</Name>  
                    <Street>123 Maple Street</Street>  
                    <City>Mill Valley</City>  
                    <State>CA</State>  
                    <Zip>10999</Zip>  
                    <Country>USA</Country>  
                  </Address>  
                  <Address Type=\""Billing\"">  
                    <Name>Tai Yee</Name>  
                    <Street>8 Oak Avenue</Street>  
                    <City>Old Town</City>  
                    <State>PA</State>  
                    <Zip>95819</Zip>  
                    <Country>USA</Country>  
                  </Address>  
                  <DeliveryNotes>Please leave packages in shed by driveway.</DeliveryNotes>  
                  <Items>  
                    <Item PartNumber=\""872-AA\"">  
                      <ProductName>Lawnmower</ProductName>  
                      <Quantity>1</Quantity>  
                      <USPrice>148.95</USPrice>  
                      <Comment>Confirm this is electric</Comment>  
                    </Item>  
                    <Item PartNumber=\""926-AA\"">  
                      <ProductName>Baby Monitor</ProductName>  
                      <Quantity>2</Quantity>  
                      <USPrice>39.98</USPrice>  
                      <ShipDate>1999-05-21</ShipDate>  
                    </Item>  
                  </Items>  
                </PurchaseOrder>
                "
            );

            XElement purchaseOrder = XElement.Load("Test.xml");

            IEnumerable<string> partNos =
                from item in purchaseOrder.Descendants("Item")
                select (string)item.Attribute("PartNumber");

            IEnumerable<XElement> pricesByPartNos =
                from item in purchaseOrder.Descendants("Item")
                where (int)item.Element("Quantity") * (decimal)item.Element("USPrice") > 100
                orderby (string)item.Element("PartNumber")
                select item;

            IEnumerable<XElement> pricesByPartNos2 = purchaseOrder
                .Descendants("Item")
                .Where(item => (int)item.Element("Quantity") * (decimal)item.Element("USPrice") > 100)
                .OrderBy(order => order.Element("PartNumber"));
        }
    }
}
