﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tutorial.Extensions;

namespace Tutorial.Linq
{
    class LinqSelectTest
    {
        public LinqSelectTest()
        {
        }

        public static void Run()
        {
            Console.WriteLine("[ LinqSelectTest ]");

            var test = new LinqSelectTest();
            test.Select();
        }

        public void Select()
        {
            Console.WriteLine("- Select");

            var arrays = new int[][]
            {
                new int [] { 1, 2, 3 },
                new int [] { 4, 5, 6 },
                new int [] { 7, 8, 9 },
            };

            var lists = new List<List<int>>
            {
                new List<int> { 1, 2, 3 },
                new List<int> { 4, 5, 6 },
                new List<int> { 7, 8, 9 },
            };

            var r1 = arrays.SelectMany(x => { return x; });
            Console.WriteLine("Array : count {0}", r1.Count());
            r1.Print();

            var r2 = lists.SelectMany(x => x);
            Console.WriteLine("List : count {0}", r2.Count());
            r2.Print();

            Console.WriteLine(Environment.NewLine);
        }
    }
}
