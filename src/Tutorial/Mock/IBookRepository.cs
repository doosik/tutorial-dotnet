﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tutorial.Mock
{
    public interface IBookRepository
    {
        List<Book> GetBooks();
        bool AddBook(Book book);
    }
}
