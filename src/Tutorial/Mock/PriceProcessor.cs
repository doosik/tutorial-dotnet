﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tutorial.Mock
{
    public class PriceProcessor
    {
        public PriceProcessor(IBookRepository bookRepository)
        {
            bookRepo = bookRepository;
        }

        public int GetTotalPrice()
        {
            int result = 0;

            foreach (Book b in bookRepo.GetBooks())
            {
                result += b.Price;
            }

            return result;
        }

        private IBookRepository bookRepo;
    }
}
