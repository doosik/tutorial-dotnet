﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tutorial.Mock
{
    public class BestSellers
    {
        public BestSellers(IBookRepository bookRepository)
        {
            bookRepo = bookRepository;
        }

        public Result AddBook(Book book)
        {
            if (book.Name.Contains("<") || book.Name.Contains(">"))
            {
                return Result.XssAlert;
            }

            bookRepo.AddBook(book);

            return Result.Success;
        }

        IBookRepository bookRepo;
    }
}
