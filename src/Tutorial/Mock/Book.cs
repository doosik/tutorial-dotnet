﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tutorial.Mock
{
    public class Book
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string ISBN { get; set; }
    }
}
