﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;

namespace Tutorial.MySql
{
    class MySqlTest
    {
        private static string connectionString = @"
            Server=192.168.137.11;
            Database=test;
            User Id=root;
            Password=;
            AutoEnlist=false;
            SslMode=None;
            Allow User Variables=True;
            Command Timeout=10;
        ";

        public static void Run()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            /*
            Console.WriteLine($"connection string : [{connectionString.Replace(Environment.NewLine, "")}]");
            Console.WriteLine($"connection string : [{connectionString.Trim(Environment.NewLine.ToCharArray())}]");
            */
            var cs = Regex.Replace(connectionString, @"\s{2,}", String.Empty);
            Console.WriteLine($"connection string : [{cs}]");
            using (var connection = new MySqlConnection(cs))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "TIMEOUT_SLEEP";
                    command.CommandType = CommandType.StoredProcedure;
                    //command.CommandTimeout = 2;

                    command.Parameters.AddWithValue("@_SECONDS", 100);
                    command.Parameters["@_SECONDS"].Direction = ParameterDirection.Input;

                    command.ExecuteNonQuery();
                    /*
                    using (var reader = command.ExecuteReader())
                    {
                        bool read = reader.Read();
                        Console.WriteLine("Read " + read);
                    }
                    */
                }
            }

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
        }
    }
}
