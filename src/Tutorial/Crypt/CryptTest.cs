﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;
using Tutorial.Extensions;

namespace Tutorial.Crypt
{
    class CryptTest : IDisposable
    {
        public static void Run()
        {
            Console.WriteLine("[ CryptTest ]");

            using (var test = new CryptTest())
            {
                test.Decrypt();
            }
        }

        public void Dispose()
        {
        }

        public void Decrypt()
        {
            Console.WriteLine("- Decrypt");

            var enc = "4ED06E97F524517D63D0B41C3622C128|547E99DCCEBE3B67A11953C61C62D273A56AD743042CE14B73989B096E8D58D8695B9C97860411A882718F1C6CF5378E";
            var dec = CryptKey.DecryptKey(enc);

            Console.WriteLine("\t{0}", dec);
        }
    }
}
