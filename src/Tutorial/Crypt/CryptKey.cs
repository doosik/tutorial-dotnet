﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Tutorial.Extensions;
using System.Text.RegularExpressions;

namespace Tutorial.Crypt
{
    /// <summary title="CryptKey">
    /// 1. Create Date 	: 20xx.xx.xx
    /// 2. Creator 		: Unknown
    /// 3. Description 	: CryptKey
    /// 4. Precaution 	: 
    /// 5. History 		: 2019.05.15(DucThai) A19_01513 - 중복 오류로 집계되지 않는 문제 개선
    /// 6. MenuPath 	:  
    /// 7. OldName 		: 
    /// </summary>
    public class CryptKey
    {
        internal static RijndaelManaged Crypto;

        public static readonly string TripleDES;        // for DES or XOR Shift
        public static readonly string Rihndael;         // for AES 
        public static readonly string LoginR;           // for login cookie or XOR Shift
        public static readonly string FNEncryption1;    // for sql extention of FNEncrypt
        public static readonly string FNEncryption2;    // for sql extention of FNEncrypt
        public static readonly string FNEncryption3;    // for sql extention of FNEncrypt

        private static Regex _hexPrefixRegex = new Regex("(\\\\x|0x|%)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        /// <summary>
        /// Converts hex string to a byte array
        /// </summary>
        /// <param name="Input">Input string</param>
        /// <returns>A byte array equivalent of the base 64 string</returns>
        public static byte[] DecodeHex(string source)
        {
            if (source == null) return ECConst.EmptyByteArray;
            string str = _hexPrefixRegex.Replace(source, "");
            if ((str.Length % 2) != 0) return ECConst.EmptyByteArray;
            return Enumerable.Range(0, str.Length / 2)
                    .Select(x => Byte.Parse(str.Substring(2 * x, 2), NumberStyles.HexNumber))
                    .ToArray();
        }

        /// <summary>
        /// Converts a byte array to a string
        /// </summary>
        /// <param name="Input">input array</param>
        /// <param name="EncodingUsing">The type of encoding the string is using (defaults to UTF8)</param>
        /// <param name="Count">Number of bytes starting at the index to convert (use -1 for the entire array starting at the index)</param>
        /// <returns>string of the byte array</returns>
        public static string ToString(byte[] source, Encoding EncodingUsing = null, int nIndex = 0, int nCount = -1)
        {
            if (source == null || source.Length == 0) return string.Empty;
            if (EncodingUsing == null) EncodingUsing = Encoding.UTF8;
            return EncodingUsing.GetString(source, nIndex, nCount > 0 ? nCount : source.Length);
        }

        static CryptKey()
        {
            string keyFile = @"E:\Projects\tutorial\tutorial-dotnet\src\Tutorial\Crypt\ecount.key";
            string keyFile_sec = @"d:\WebService\ecount.key";
            if (!File.Exists(keyFile)) {
                if (File.Exists(keyFile_sec))
                    keyFile = keyFile_sec;
                else
                    throw new FileNotFoundException("File not found for CryptKey.", keyFile);
            }

            XmlDocument xmldom = new XmlDocument();
            xmldom.LoadXml(File.ReadAllText(keyFile));

            Crypto = new RijndaelManaged();
            var text = xmldom.DocumentElement.SelectSingleNode("SALT").InnerText;
            Crypto.Key = DecodeHex(text);

            TripleDES = DecryptKey(xmldom.DocumentElement.SelectSingleNode("DES").InnerText);
            Rihndael = DecryptKey(xmldom.DocumentElement.SelectSingleNode("AES").InnerText);
            LoginR = DecryptKey(xmldom.DocumentElement.SelectSingleNode("LoginR").InnerText);
            FNEncryption1 = DecryptKey(xmldom.DocumentElement.SelectSingleNode("FNEnc1").InnerText);
            FNEncryption2 = DecryptKey(xmldom.DocumentElement.SelectSingleNode("FNEnc2").InnerText);
            FNEncryption3 = DecryptKey(xmldom.DocumentElement.SelectSingleNode("FNEnc3").InnerText);
        }
        
        public static string DecryptKey(string keyDataValue)
        {
            try {
                var arrKeyData = keyDataValue.Split('|');
                if (arrKeyData.Length != 2) {
                    return keyDataValue;
                }

                var encryptedKey = DecodeHex(arrKeyData[1]);
                var plainTextBytes = new byte[encryptedKey.Length];
                int decryptedCount = 0;
                        
                using (var ms = new System.IO.MemoryStream(encryptedKey)) {
                    Crypto.IV = DecodeHex(arrKeyData[0]);

                    using (var cs = new CryptoStream(ms, Crypto.CreateDecryptor(), CryptoStreamMode.Read)) {
                        decryptedCount = cs.Read(plainTextBytes, 0, plainTextBytes.Length);
                    }
                }

                return ToString(plainTextBytes, ECConst.ANSIEncoding, 0, decryptedCount);
            }
            catch (Exception ex) {
                throw new Exception("Fail to decrypt", new Exception(String.Format("Fail to decrypt = {0}: {1}", keyDataValue, ex.Message)));
            }
        }
    }

    public class Cryptograph : IDisposable
    {
        string m_strKey = CryptKey.TripleDES;
        Symmetric.Provider m_p = Symmetric.Provider.TripleDES;

        public Cryptograph()
        {
        }

        public string Encrypt(string context)
        {
            Symmetric sym = new Symmetric(m_p, true);

            sym.Key.Text = m_strKey;

            Data encryptedData;
            encryptedData = sym.Encrypt(new Data(context));

            return encryptedData.ToBase64();
        }

        public string Decrypt(string context)
        {
            Symmetric sym2 = new Symmetric(m_p, true);

            Data decryptedData = new Data();

            decryptedData.Base64 = context;

            sym2.Key.Text = m_strKey;
            decryptedData = sym2.Decrypt(decryptedData);

            return decryptedData.Text;
        }

        /// <summary>
        /// 암/복호화에 사용할 Vector Key 의 값을 가져오거나 설정할 수 있습니다.
        /// </summary>
        public string VectorKey
        {
            get
            {
                return m_strKey;
            }
            set
            {
                m_strKey = value;
            }
        }

        #region IDisposable 멤버

        public void Dispose()
        {

        }

        #endregion
    }
}
