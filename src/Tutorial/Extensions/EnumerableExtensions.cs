﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial.Extensions
{
    public static class EnumerableExtensions
    {
        public static void Print<T>(this IEnumerable<T> source)
        {
            int index = 0;
            foreach (T item in source)
            {
                if (index > 0) Console.Write(", ");
                Console.Write($"[{index}] {item.ToString()}");
                ++index;
            }
            Console.WriteLine();
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T item in source) {
                action(item);
            }
        }

        public static IEnumerable<R> Map<T, R>(this IEnumerable<T> source, Func<T, R> func)
        {
            foreach (T item in source) {
                yield return func(item);
            }
        }
    }
}
