﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial.Extensions
{
    public static class ArrayExtensions
    {
        public static void Print<T>(this T[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (i > 0) Console.Write(", ");
                Console.Write($"[{i}] {array[i]}");
            }
            Console.WriteLine();
        }

        public static int[] MergeSort(this int[] array)
        {
            return RecursiveMergeSort(array, 0, array.Length - 1);
        }

        private static int[] RecursiveMergeSort(this int[] array, int left, int right)
        {
            int _right;
            int mid = (left + right) / 2;
            if (left < right)
            {
                //좌측찢기
                _right = mid;
                int[] leftarray = new int[_right + 1];
                for (int i = 0; i <= mid; i++)
                {
                    leftarray[i] = array[i];
                }
                int[] _leftarray = RecursiveMergeSort(leftarray, 0, mid);


                //우측찢기
                left = mid + 1;
                int[] rightarray = new int[right - left + 1];
                int j = mid + 1;
                for (int i = 0; i < right - mid; j++, i++)
                {
                    rightarray[i] = array[j];
                }
                int[] _rightarray = RecursiveMergeSort(rightarray, 0, rightarray.Length - 1);

                return Merge(_leftarray, _rightarray);
            }

            return array;

        }

        private static int[] Merge(int[] leftarray, int[] rightarray)
        {
            int[] temp = new int[(leftarray.Length + rightarray.Length)];

            int i = 0;
            int j = 0;
            int k = 0;
            for (; k < temp.Length; k++)
            {

                if (j >= rightarray.Length)
                {
                    temp[k] = leftarray[i];
                    i++;
                }
                else if (i >= leftarray.Length)
                {
                    temp[k] = rightarray[j];
                    j++;
                }
                else if (leftarray[i] < rightarray[j])
                {
                    temp[k] = leftarray[i];
                    i++;
                }
                else if (leftarray[i] > rightarray[j])
                {
                    temp[k] = rightarray[j];
                    j++;
                }
            }

            return temp;
        }
    }
}
