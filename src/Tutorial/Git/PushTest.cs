﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;
using Tutorial.Extensions;

namespace Tutorial.Git
{
    class PushTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\push-test";

        public static void Run()
        {
            Console.WriteLine("[ PushTest ]");

            using (var test = new PushTest())
            {
                string url = "http://git.ecount.kr/tutorial/helloworld.git";
                using (var repo = GitHelper.CloneRepository(url, test.workDir))
                {
                    test.MakeCommit(repo);
                    test.Push(repo);
                    //test.PushUsingRemote(repo);
                }
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void Push(Repository repo)
        {
            Console.WriteLine("- Push");

            // $ git push

            var pushOptions = new PushOptions
            {
                /*
                CredentialsProvider = (url, user, cred) =>
                {
                    return new UsernamePasswordCredentials
                    {
                        Username = "Username",
                        Password = "Password"
                    };
                }
                */
            };

            repo.Network.Push(repo.Branches["master"], pushOptions);
        }

        public void PushUsingRemote(Repository repo)
        {
            Console.WriteLine("- PushUsingRemote");

            // $ git push

            var pushOptions = new PushOptions
            {
                /*
                CredentialsProvider = (url, user, cred) =>
                {
                    return new UsernamePasswordCredentials
                    {
                        Username = "Username",
                        Password = "Password"
                    };
                }
                */
            };
            Remote remote = repo.Network.Remotes["origin"];

            repo.Network.Push(remote, "refs/heads/master", pushOptions);
        }

        public void MakeCommit(Repository repo)
        {
            Console.WriteLine("- MakeCommit");

            // make the file
            var file = Path.Combine(repo.Info.WorkingDirectory, "Test.txt");
            var message = DateTime.Now.ToString();
            File.WriteAllText(file, message);

            // stage the file
            repo.Index.Add(Path.GetFileName(file));
            repo.Index.Write();

            // create the committer's signature and commit
            var author = new Signature("2018-059 KangDooSik", "18059doosik@ecount.kr", DateTime.Now);
            var committer = author;

            // commit to the repository
            Commit commit = repo.Commit(message, author, committer);
            Console.WriteLine($"\tcommit sha={commit.Sha}");
        }
    }
}
