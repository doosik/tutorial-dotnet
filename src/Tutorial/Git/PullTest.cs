﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;

namespace Tutorial.Git
{
    class PullTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\pull-test";

        public static void Run()
        {
            Console.WriteLine("[ PullTest ]");

            using (var test = new PullTest())
            {
                test.PullIntoEmptyRepository();
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void PullIntoEmptyRepository()
        {
            string remoteName = "origin";
            string remoteUrl = "http://git.ecount.kr/tutorial/helloworld.git";

            string fetchRefSpec = "+refs/heads/*:refs/remotes/origin/*";
            string upstreamBranchName = "refs/heads/master";

            using (var repo = GitHelper.InitRepository(workDir))
            {
                Console.WriteLine("- PullIntoEmptyRepository");

                repo.Network.Remotes.Add(remoteName, remoteUrl, fetchRefSpec);
                Remote remote = repo.Network.Remotes[remoteName];
                Console.WriteLine($"\tremote name={remote.Name}");
                Console.WriteLine($"\tremote url={remote.Url}");

                Branch updatedBranch = repo.Branches.Update(repo.Head,
                    b => b.Remote = remoteName,
                    b => b.UpstreamBranch = upstreamBranchName);

                var sign = new Signature("2018-059 KangDooSik", "18059doosik@ecount.kr", DateTimeOffset.Now);
                var fetchOptions = new FetchOptions
                {
                    TagFetchMode = TagFetchMode.None,
                    OnUpdateTips = OnUpdateTipsHandler
                };
                var mergeOptions = new MergeOptions
                {
                    FastForwardStrategy = FastForwardStrategy.Default
                };
                var pullOptions = new PullOptions
                {
                    //FetchOptions = fetchOptions,
                    MergeOptions = mergeOptions
                };

                Commands.Pull(repo, sign, pullOptions);
            }
        }

        private bool OnUpdateTipsHandler(string referenceName, ObjectId oldId, ObjectId newId)
        {
            Console.WriteLine("\t- OnUpdateTipsHandler");
            Console.WriteLine($"\t\treferenceName={referenceName}");
            Console.WriteLine($"\t\toldId={oldId}");
            Console.WriteLine($"\t\tnewId={newId}");

            return true;
        }
    }
}
