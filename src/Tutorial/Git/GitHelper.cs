﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;

namespace Tutorial.Git
{
    public static class GitHelper
    {
        public static Repository InitRepository(string workDir)
        {
            Console.WriteLine("* InitRepository");

            string repoDir = Path.Combine(workDir, ".git");
            string initedRepoDir = Repository.Init(repoDir);
            var repo = new Repository(initedRepoDir);

            Console.WriteLine($"\trepo.Info.Path={repo.Info.Path}");
            Console.WriteLine($"\trepo.Info.WorkingDirectory={repo.Info.WorkingDirectory}");

            return repo;
        }

        public static Repository CloneRepository(string remoteUrl, string workDir)
        {
            Console.WriteLine("* CloneRepository");

            /*
            var options = new CloneOptions()
            {
                //IsBare = false,
                //Checkout = true,
                BranchName = "master",
                //RecurseSubmodules = true,
                //OnCheckoutProgress = null,
                //FetchOptions = null,
                CredentialsProvider = (url, user, cred) =>
                {
                    return new UsernamePasswordCredentials
                    {
                        Username = "Username",
                        Password = "Password"
                    };
                }
            };
            */
            string clonedRepoDir= Repository.Clone(remoteUrl, workDir/*, options*/);
            var repo = new Repository(clonedRepoDir);

            Console.WriteLine($"\trepo.Info.Path={repo.Info.Path}");
            Console.WriteLine($"\trepo.Info.WorkingDirectory={repo.Info.WorkingDirectory}");

            return repo;
        }

        public static void SetAttributes(string path, FileAttributes attrs)
        {
            string[] files = Directory.GetFiles(path);
            string[] subdirs = Directory.GetDirectories(path);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
            }

            foreach (string subdir in subdirs)
            {
                SetAttributes(subdir, attrs);
            }

            File.SetAttributes(path, FileAttributes.Normal);
        }

        public static void DeleteDirectory(string path)
        {
            if (!Directory.Exists(path)) return;
            SetAttributes(path, FileAttributes.Normal);
            Directory.Delete(path, true);
        }
    }
}
