﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;
using Tutorial.Extensions;
using System.Globalization;

namespace Tutorial.Git
{
    class LogTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\log-test";

        public static void Run()
        {
            Console.WriteLine("[ LogTest ]");

            using (var test = new LogTest())
            {
                string url = "http://git.ecount.kr/tutorial/helloworld.git";
                using (var repo = GitHelper.CloneRepository(url, test.workDir))
                {
                    test.MakeCommit(repo);
                    test.ShowCommit(repo);
                    test.ShowCommit_ChangeOrder(repo);
                }
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void ShowCommit(Repository repo)
        {
            Console.WriteLine("- ShowCommit");

            // $ git log -15

            var RFC2822Format = "ddd dd MMM HH:mm:ss yyyy K";

            foreach (Commit c in repo.Commits.Take(15))
            {
                Console.WriteLine(string.Format("commit {0}", c.Id));

                if (c.Parents.Count() > 1)
                {
                    Console.WriteLine("Merge: {0}",
                        string.Join(" ", c.Parents.Select(p => p.Id.Sha.Substring(0, 7)).ToArray()));
                }

                Console.WriteLine(string.Format("Author: {0} <{1}>", c.Author.Name, c.Author.Email));
                Console.WriteLine("Date:   {0}", c.Author.When.ToString(RFC2822Format, CultureInfo.InvariantCulture));
                Console.WriteLine();
                Console.WriteLine(c.Message);
                Console.WriteLine();
            }
        }

        public void ShowCommit_ChangeOrder(Repository repo)
        {
            Console.WriteLine("- ShowCommit_ChangeOrder");

            // $ git log -1

            var filter = new CommitFilter
            {
                SortBy = CommitSortStrategies.Topological | CommitSortStrategies.Reverse
            };

            foreach (Commit c in repo.Commits.QueryBy(filter))
            {
                Console.WriteLine("\t{0}", c.Id);
            }
        }

        /*
        public void ShowCommit_BetweenTwoNamedCommits(Repository repo)
        {
            Console.WriteLine("- ShowCommit_BetweenTwoCommits");

            // $ git log af92829..dabb6b9

            var filter = new CommitFilter
            {
                ExcludeReachableFrom = repo.Branches["master"],       // formerly "Since"
                IncludeReachableFrom = repo.Branches["development"],  // formerly "Until"
            };

            foreach (Commit c in repo.Commits.QueryBy(filter))
            {
                Console.WriteLine("\t{0}", c.Id);
            }
        }
        */
        /*
        public void ShowCommit_BetweenTwoTags(Repository repo)
        {
            Tag tagTo = repo.Tags["tag2"];
            Tag tagFrom = repo.Tags["tag1"];

            // $ git log tag1 tag2

            var filter = new CommitFilter()
            {
                IncludeReachableFrom = tagTo.Target.Sha,
                ExcludeReachableFrom = tagFrom.Target.Sha
            };

            foreach (Commit c in repo.Commits.QueryBy(filter))
            {
                Console.WriteLine("\t{0}", c.Id);
            }
        }
        */
        /*
        public void ShowTreeChanges_BetweenTwoCommits(Repository repo)
        {
            Tag tagTo = repo.Tags["tag2"];
            Tag tagFrom = repo.Tags["tag1"];

            // $ git log tag1 tag2 --stat

            var commitFrom = repo.Lookup<LibGit2Sharp.Commit>(tagFrom.Target.Sha);
            var commitTo = repo.Lookup<LibGit2Sharp.Commit>(tagTo.Target.Sha);

            TreeChanges treeChanges = repo.Diff.Compare<TreeChanges>(commitFrom.Tree, commitTo.Tree);
        }
        */

        private void MakeCommit(Repository repo)
        {
            Console.WriteLine("- MakeCommit");

            // make the file
            var file = Path.Combine(repo.Info.WorkingDirectory, "Test.txt");
            File.WriteAllText(file, "Commit Test!");

            // stage the file
            repo.Index.Add(Path.GetFileName(file));
            repo.Index.Write();

            // create the committer's signature and commit
            var author = new Signature("2018-059 KangDooSik", "18059doosik@ecount.kr", DateTime.Now);
            var committer = author;

            // commit to the repository
            Commit commit = repo.Commit("add test.txt", author, committer);
            Console.WriteLine($"\tcommit sha={commit.Sha}");
        }
    }
}
