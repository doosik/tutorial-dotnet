﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;
using Tutorial.Extensions;

namespace Tutorial.Git
{
    class FetchTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\fetch-test";

        public static void Run()
        {
            Console.WriteLine("[ FetchTest ]");

            using (var test = new FetchTest())
            {
                test.FetchIntoEmptyRepository();
            }

            using (var test = new FetchTest())
            {
                test.FetchAllIntoCloneRepository();
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void FetchIntoEmptyRepository()
        {
            string remoteName = "origin";
            string remoteUrl = "http://git.ecount.kr/tutorial/helloworld.git";

            string fetchRefSpec = "+refs/heads/*:refs/remotes/origin/*";
            //string upstreamBranchName = "refs/heads/master";

            using (var repo = GitHelper.InitRepository(workDir))
            {
                Console.WriteLine("- FetchIntoEmptyRepository");

                repo.Network.Remotes.Add(remoteName, remoteUrl, fetchRefSpec);
                Remote remote = repo.Network.Remotes[remoteName];
                Console.WriteLine($"\tremote name={remote.Name}");
                Console.WriteLine($"\tremote url={remote.Url}");

                /*
                Branch updatedBranch = repo.Branches.Update(repo.Head,
                    b => b.Remote = remoteName,
                    b => b.UpstreamBranch = upstreamBranchName);
                */

                var refSpecs = remote.FetchRefSpecs.Select(x => x.Specification);
                Console.WriteLine("\trefSpecs :");
                refSpecs.ForEach(rs =>
                {
                    Console.WriteLine($"\t\t{rs}");
                });

                var fetchOptions = new FetchOptions
                {
                    /*
                    CredentialsProvider = (url, user, cred) =>
                    {
                        return new UsernamePasswordCredentials
                        {
                            Username = "Username",
                            Password = "Password"
                        };
                    },
                    */
                    TagFetchMode = TagFetchMode.None,
                    OnUpdateTips = OnUpdateTipsHandler
                };

                //Commands.Fetch(repo, remoteName, new string[0], fetchOptions, null);
                Commands.Fetch(repo, remote.Name, refSpecs, fetchOptions, "");
            }
        }

        public void FetchAllIntoCloneRepository()
        {
            string remoteUrl = "http://git.ecount.kr/tutorial/helloworld.git";

            using (var repo = GitHelper.CloneRepository(remoteUrl, workDir))
            {
                Console.WriteLine("- FetchAllIntoCloneRepository");

                // $ git fetch --all

                var fetchOptions = new FetchOptions
                {
                    /*
                    CredentialsProvider = (url, user, cred) =>
                    {
                        return new UsernamePasswordCredentials
                        {
                            Username = "Username",
                            Password = "Password"
                        };
                    },
                    */
                    TagFetchMode = TagFetchMode.None,
                    OnUpdateTips = OnUpdateTipsHandler
                };

                repo.Network.Remotes.ForEach(remote =>
                {
                    Console.WriteLine($"\tremote name={remote.Name}");
                    Console.WriteLine($"\tremote url={remote.Url}");

                    var refSpecs = remote.FetchRefSpecs.Select(x => x.Specification);
                    Console.WriteLine("\trefSpecs :");
                    refSpecs.ForEach(rs =>
                    {
                        Console.WriteLine($"\t\t{rs}");
                    });

                    Commands.Fetch(repo, remote.Name, refSpecs, fetchOptions, "");
                });
            }
        }

        private bool OnUpdateTipsHandler(string referenceName, ObjectId oldId, ObjectId newId)
        {
            Console.WriteLine("\t- OnUpdateTipsHandler");
            Console.WriteLine($"\t\treferenceName={referenceName}");
            Console.WriteLine($"\t\toldId={oldId}");
            Console.WriteLine($"\t\tnewId={newId}");

            return true;
        }
    }
}
