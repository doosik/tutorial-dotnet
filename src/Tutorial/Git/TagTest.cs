﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;
using Tutorial.Extensions;

namespace Tutorial.Git
{
    class TagTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\tag-test";

        public static void Run()
        {
            Console.WriteLine("[ TagTest ]");

            using (var test = new TagTest())
            {
                string url = "http://git.ecount.kr/tutorial/helloworld.git";
                using (var repo = GitHelper.CloneRepository(url, test.workDir))
                {
                    test.ApplyTag_PointingAtTheCurrentHEAD(repo);
                    test.ApplyTag_PointingAtTheASpecificTarget(repo);
                    test.PrintAllTags(repo);
                }
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void ApplyTag_PointingAtTheCurrentHEAD(Repository repo)
        {
            Console.WriteLine("- ApplyTag_PointingAtTheCurrentHEAD");

            // $ git tag <tag-name>

            Tag t = repo.ApplyTag("tag-12345");
            Console.WriteLine("\t{0}  {1}", t.FriendlyName, t.Target.Sha);
        }

        public void ApplyTag_PointingAtTheASpecificTarget(Repository repo)
        {
            Console.WriteLine("- ApplyTag_PointingAtTheASpecificTarget");

            // $ git tag <tag-name> <>

            Tag t = repo.ApplyTag("tag-11111", "refs/heads/master");
            Console.WriteLine("\t{0}  {1}", t.FriendlyName, t.Target.Sha);

            Tag t2 = repo.ApplyTag("tag-22222", "00ffcf2c3463c5ec2529e05c532a0443cee27e12");
            Console.WriteLine("\t{0}  {1}", t2.FriendlyName, t2.Target.Sha);
        }

        public void PrintAllTags(Repository repo)
        {
            Console.WriteLine("- PrintAllTags");

            // $ git tag

            foreach (Tag t in repo.Tags)
            {
                Console.WriteLine("\t{0}  {1}", t.FriendlyName, t.Target.Sha);
            }
        }
    }
}
