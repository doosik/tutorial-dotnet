﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;
using Tutorial.Extensions;

namespace Tutorial.Git
{
    class StatusTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\status-test";

        public static void Run()
        {
            Console.WriteLine("[ StatusTest ]");

            using (var test = new StatusTest())
            {
                string url = "http://git.ecount.kr/tutorial/helloworld.git";
                using (var repo = GitHelper.CloneRepository(url, test.workDir))
                {
                    test.AddFile(repo);
                    test.PrintRepositoryStatus(repo);
                }
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void PrintRepositoryStatus(Repository repo)
        {
            Console.WriteLine("- PrintRepositoryStatus");

            // $ git status

            foreach (var item in repo.RetrieveStatus(new StatusOptions()))
            {
                Console.WriteLine("\t{0}  {1}", item.FilePath, item.State);
            }
        }

        public void AddFile(Repository repo)
        {
            Console.WriteLine("- AddFile");

            // make the file
            var file = Path.Combine(repo.Info.WorkingDirectory, "Test.txt");
            File.WriteAllText(file, "Commit Test!");

            // stage the file
            repo.Index.Add(Path.GetFileName(file));
            repo.Index.Write();
        }
    }
}
