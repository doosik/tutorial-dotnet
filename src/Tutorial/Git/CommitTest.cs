﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;

namespace Tutorial.Git
{
    class CommitTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\commit-test";

        public static void Run()
        {
            Console.WriteLine("[ BranchTest ]");

            using (var test = new CommitTest())
            {
                string url = "http://git.ecount.kr/tutorial/helloworld.git";
                using (var repo = GitHelper.CloneRepository(url, test.workDir))
                {
                    test.MakeCommit(repo);
                }
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void MakeCommit(Repository repo)
        {
            Console.WriteLine("- MakeCommit");

            // make the file
            var file = Path.Combine(repo.Info.WorkingDirectory, "Test.txt");
            File.WriteAllText(file, "Commit Test!");

            // stage the file
            repo.Index.Add(Path.GetFileName(file));
            repo.Index.Write();

            // create the committer's signature and commit
            var author = new Signature("2018-059 KangDooSik", "18059doosik@ecount.kr", DateTime.Now);
            var committer = author;

            // commit to the repository
            Commit commit = repo.Commit("add test.txt", author, committer);
            Console.WriteLine($"\tcommit sha={commit.Sha}");
        }
    }
}
