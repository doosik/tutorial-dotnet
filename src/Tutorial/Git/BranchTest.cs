﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;

namespace Tutorial.Git
{
    class BranchTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\branch-test";

        public static void Run()
        {
            Console.WriteLine("[ BranchTest ]");

            using (var test = new BranchTest())
            {
                string url = "http://git.ecount.kr/tutorial/helloworld.git";
                using (var repo = GitHelper.CloneRepository(url, test.workDir))
                {
                    test.CreateBranch_PointingAtTheCurrentHEAD(repo);
                    test.CreateBranch_PointingAtASpecificRevision(repo);
                    test.PrintLocalBranches(repo);
                    test.DeleteBranch(repo);
                    test.EditBranchDescription(repo);
                    test.CheckoutBranch(repo);
                    test.PrintAllBranches(repo);
                }
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void PrintAllBranches(Repository repo)
        {
            Console.WriteLine("- PrintAllBranches");

            // $ git branch -a
            foreach (Branch b in repo.Branches)
            {
                Console.WriteLine("\t{0} {1}", b.FriendlyName, b.IsCurrentRepositoryHead ? "*" : "");
            }
        }

        public void PrintLocalBranches(Repository repo)
        {
            Console.WriteLine("- PrintLocalBranches");

            // $ git branch
            foreach (Branch b in repo.Branches.Where(b => !b.IsRemote))
            {
                Console.WriteLine("\t{0} {1}", b.FriendlyName, b.IsCurrentRepositoryHead ? "*" : "");
            }
        }

        public void CreateBranch_PointingAtTheCurrentHEAD(Repository repo)
        {
            Console.WriteLine("- CreateBranch_PointingAtTheCurrentHEAD");

            // $ git branch develop
            repo.CreateBranch("develop");
            // Or repo.Branches.Add("develop", "HEAD");
        }

        public void CreateBranch_PointingAtASpecificRevision(Repository repo)
        {
            Console.WriteLine("- CreateBranch_PointingAtASpecificRevision");

            // $ git brach other HEAD~1
            // repo.CreateBranch("other", "HEAD~1");
            // Or repo.Branches.Add("other", "HEAD~1");
            Assert.Throws<NotFoundException>(() => repo.CreateBranch("other", "HEAD~1"));
        }

        public void DeleteBranch(Repository repo)
        {
            Console.WriteLine("- DeleteBranch");

            repo.Branches.Remove("other");
        }

        public void EditBranchDescription(Repository repo)
        {
            Console.WriteLine("- EditBranchDescription");

            // $ git branch --edit-description
            var description = "branch description";
            var branch = "other";
            var key = string.Format("branch.{0}.description", branch);
            repo.Config.Set(key, description.Replace(Environment.NewLine, string.Empty)); // set description
            repo.Config.Unset(key); // remove description
        }

        public void CheckoutBranch(Repository repo)
        {
            Console.WriteLine("- CheckoutBranch");

            // $ git checkout <branch>
            var branch = repo.Branches["develop"];
            if (branch != null)
            {
                Branch currentBranch = Commands.Checkout(repo, branch);
                Console.WriteLine($"\tcrrent branch={currentBranch.CanonicalName}");
            }
        }
    }
}
