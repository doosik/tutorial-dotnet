﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;
using Tutorial.Extensions;

namespace Tutorial.Git
{
    class ResetTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\reset-test";

        public static void Run()
        {
            Console.WriteLine("[ ResetTest ]");

            using (var test = new ResetTest())
            {
                string url = "http://git.ecount.kr/tutorial/helloworld.git";
                using (var repo = GitHelper.CloneRepository(url, test.workDir))
                {
                    test.MakeCommit(repo);
                    test.UndoLastCommit(repo);
                }
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void UndoLastCommit(Repository repo)
        {
            Console.WriteLine("- UndoLastCommit");

            // $ git reset --soft HEAD^

            Commit lastCommit = repo.Head.Commits.ElementAt(0);
            Commit previousCommit = repo.Head.Commits.ElementAt(1);
            Console.WriteLine($"\tlast commit sha={lastCommit.Sha}");
            Console.WriteLine($"\tprevious commit sha={previousCommit.Sha}");
            repo.Reset(ResetMode.Soft, previousCommit);

            repo.Head.Commits.ForEach(commit =>
            {
                Console.WriteLine($"\tcommit {commit.Sha}");
                Console.WriteLine($"\tauthor: {commit.Author}");
                Console.WriteLine($"\tmessage: {commit.Message}");
            });
        }

        private void MakeCommit(Repository repo)
        {
            Console.WriteLine("- MakeCommit");

            // make the file
            var file = Path.Combine(repo.Info.WorkingDirectory, "Test.txt");
            File.WriteAllText(file, "Commit Test!");

            // stage the file
            repo.Index.Add(Path.GetFileName(file));
            repo.Index.Write();

            // create the committer's signature and commit
            var author = new Signature("2018-059 KangDooSik", "18059doosik@ecount.kr", DateTime.Now);
            var committer = author;

            // commit to the repository
            Commit commit = repo.Commit("add test.txt", author, committer);
            Console.WriteLine($"\tcommit sha={commit.Sha}");
        }
    }
}
