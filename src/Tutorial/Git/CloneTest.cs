﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;

namespace Tutorial.Git
{
    public class CloneTest : IDisposable
    {
        private string workDir = @"D:\Temp\git-test\clone-test";

        public static void Run()
        {
            Console.WriteLine("[ CloneTest ]");

            using (var test = new CloneTest())
            {
                test.CloneRepository();
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void CloneRepository()
        {
            Console.WriteLine("- CloneRepository");

            /*
            var options = new CloneOptions()
            {
                //IsBare = false,
                //Checkout = true,
                BranchName = "master",
                //RecurseSubmodules = true,
                //OnCheckoutProgress = null,
                //FetchOptions = null,
                CredentialsProvider = (url, user, cred) =>
                {
                    return new UsernamePasswordCredentials
                    {
                        Username = "Username",
                        Password = "Password"
                    };
                }
            };
            */

            string sourceUrl = "http://git.ecount.kr/tutorial/helloworld.git";
            Console.WriteLine($"\tsourceUrl={sourceUrl}");
            Console.WriteLine($"\tworkDir={workDir}");

            string clonedRepoDir= Repository.Clone(sourceUrl, workDir/*, options*/);
            using (var repo = new Repository(clonedRepoDir))
            {
                Console.WriteLine($"\trepo.Info.Path={repo.Info.Path}");
                Assert.Equal($@"{workDir}\.git\", repo.Info.Path);
                Assert.True(Path.IsPathRooted(repo.Info.Path));
                Assert.True(Directory.Exists(repo.Info.Path));

                Console.WriteLine($"\trepo.Info.WorkingDirectory={repo.Info.WorkingDirectory}");
                Assert.Equal($@"{workDir}\", repo.Info.WorkingDirectory);
            }
        }
    }
}
