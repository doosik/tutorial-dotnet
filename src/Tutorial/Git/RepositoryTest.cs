﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;

namespace Tutorial.Git
{
    public class RepositoryTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\repo-test";

        public static void Run()
        {
            Console.WriteLine("[ RepositoryTest ]");

            using (var test = new RepositoryTest())
            {
                test.CreateRepository();
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void CreateRepository()
        {
            Console.WriteLine("- CreateRepository");

            string repoDir = Path.Combine(workDir, ".git");

            Console.WriteLine($"\tworkDir={workDir}");
            Console.WriteLine($"\trepoDir={repoDir}");
            Assert.False(Repository.IsValid(repoDir));

            Assert.Throws<RepositoryNotFoundException>(() => new Repository(repoDir));

            // libgit2의 디렉토리 경로는 마지막에 '\\' 문자를 포함한다.
            // Path.DirectorySeparatorChar
            string initedRepoDir = Repository.Init(repoDir);
            Console.WriteLine($"\tinitedRepoDir={initedRepoDir}");
            Assert.Equal($@"{repoDir}\", initedRepoDir);
            Assert.True(Directory.Exists(initedRepoDir));
            Assert.True(Repository.IsValid(initedRepoDir));

            using (var repo = new Repository(initedRepoDir))
            {
                Console.WriteLine($"\trepo.Info.Path={repo.Info.Path}");
                Assert.Equal($@"{repoDir}\", repo.Info.Path);
                Console.WriteLine($"\trepo.Info.WorkingDirectory={repo.Info.WorkingDirectory}");
                Assert.Equal($@"{workDir}\", repo.Info.WorkingDirectory);
            }
        }
    }
}
