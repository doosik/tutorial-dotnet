﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;
using Tutorial.Extensions;

namespace Tutorial.Git
{
    class LsFilesTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\lsfiles-test";

        public static void Run()
        {
            Console.WriteLine("[ LsFilesTest ]");

            using (var test = new LsFilesTest())
            {
                string url = "http://git.ecount.kr/tutorial/helloworld.git";
                using (var repo = GitHelper.CloneRepository(url, test.workDir))
                {
                    test.PrintUnmergedFiles(repo);
                }
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void PrintUnmergedFiles(Repository repo)
        {
            Console.WriteLine("- PrintUnmergedFiles");

            // $ git ls-files --unmerged

            foreach (IndexEntry e in repo.Index)
            {
                if (e.StageLevel == 0)
                {
                    continue;
                }

                Console.WriteLine("{0} {1} {2}       {3}",
                    Convert.ToString((int)e.Mode, 8),
                    e.Id.ToString(), (int)e.StageLevel, e.Path);
            }
        }
    }
}
