﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using Xunit;
using Tutorial.Extensions;

namespace Tutorial.Git
{
    class RemoteTest : IDisposable
    {
        string workDir = @"D:\Temp\git-test\remote-test";

        public static void Run()
        {
            Console.WriteLine("[ RemoteTest ]");

            using (var test = new RemoteTest())
            {
                test.SetRemoteUrlAfterInitRepository();
            }

            using (var test = new RemoteTest())
            {
                test.PrintAllRemoteUrlAfterCloneRepository();
            }
        }

        public void Dispose()
        {
            GitHelper.DeleteDirectory(workDir);
        }

        public void SetRemoteUrlAfterInitRepository()
        {
            string name = "origin";
            string url = "http://git.ecount.kr/tutorial/helloworld-core.git";
            string newUrl = "http://git.ecount.kr/tutorial/helloworld.git";

            using (var repo = GitHelper.InitRepository(workDir))
            {
                Console.WriteLine("- SetRemoteUrlAfterInitRepository");

                Remote remote;

                // add
                repo.Network.Remotes.Add(name, url);

                // get
                remote = repo.Network.Remotes[name];
                Assert.NotNull(remote);
                Assert.Equal(url, remote.Url);
                Console.WriteLine($"\tremote name={name}");
                Console.WriteLine($"\tremote url={url}");

                // update
                repo.Network.Remotes.Update(name, r => r.Url = newUrl);
                remote = repo.Network.Remotes[name];
                Assert.NotNull(remote);
                Assert.Equal(newUrl, remote.Url);
                Console.WriteLine($"\tremote name={name}");
                Console.WriteLine($"\tremote newUrl={newUrl}");
            }
        }

        public void PrintAllRemoteUrlAfterCloneRepository()
        {
            string url = "http://git.ecount.kr/tutorial/helloworld.git";

            using (var repo = GitHelper.CloneRepository(url, workDir))
            {
                Console.WriteLine("- PrintAllRemoteUrlAfterCloneRepository");

                repo.Network.Remotes.ForEach(r =>
                {
                    Console.WriteLine($"\t{r.Name}");
                    Console.WriteLine($"\t\t{r.Url}");
                    r.RefSpecs.ForEach(rs =>
                    {
                        Console.WriteLine($"\t\t{rs.Source}");
                    });
                });
            }
        }
    }
}
