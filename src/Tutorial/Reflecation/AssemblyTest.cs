﻿using System;
using System.Reflection;
using System.Security.Permissions;

namespace Tutorial.Reflection
{
    class AssemblyTest
    {
        private int factor;

        public AssemblyTest(int f)
        {
            factor = f;
        }

        public int SampleMethod(int x)
        {
            Console.WriteLine("\nAssemblyTest.SampleMethod({0}) executes.", x);
            return x * factor;
        }

        public static void Run()
        {
            Console.WriteLine("[ AssemblyTest ]");

            Console.WriteLine(typeof(AssemblyTest));

            Assembly assem = typeof(AssemblyTest).Assembly;

            Console.WriteLine("Assembly Full Name:");
            Console.WriteLine(assem.FullName);

            // The AssemblyName type can be used to parse the full name.
            AssemblyName assemName = assem.GetName();
            Console.WriteLine("\nName: {0}", assemName.Name);
            Console.WriteLine("Version: {0}.{1}",
                assemName.Version.Major, assemName.Version.Minor);

            Console.WriteLine("\nAssembly CodeBase:");
            Console.WriteLine(assem.CodeBase);

            // Create an object from the assembly, passing in the correct number
            // and type of arguments for the constructor.
            Object o = assem.CreateInstance("Tutorial.Reflection.AssemblyTest", false,
                BindingFlags.ExactBinding,
                null, new Object[] { 2 }, null, null);

            // Make a late-bound call to an instance method of the object.    
            Type typ = assem.GetType("Tutorial.Reflection.AssemblyTest");
            MethodInfo m = assem.GetType("Tutorial.Reflection.AssemblyTest").GetMethod("SampleMethod");
            Object ret = m.Invoke(o, new Object[] { 42 });
            Console.WriteLine("SampleMethod returned {0}.", ret);

            Console.WriteLine("\nAssembly entry point:");
            Console.WriteLine(assem.EntryPoint);
        }
    }
}
