using System;
using System.Collections.Generic;
using Xunit;
using Moq;
using Tutorial.Mock;

namespace Tutorial.Tests.Mock
{
    public class PriceProcessorTest
    {
        [Fact]
        public void GetTotalPrice_NoDiscount_ReturnCorrectPriceSum()
        {
            List<Book> bookList = new List<Book>()
            {
                new Book() {ISBN="1234", Price=10000, Name="C언어 입문"},
                new Book() {ISBN="1235", Price=20000, Name="C++ 입문"},
                new Book() {ISBN="1236", Price=30000, Name="ASP.NET MVC3 완성"},
            };

            Mock<IBookRepository> bookRepoMock = new Mock<IBookRepository>();

            bookRepoMock.Setup(b => b.GetBooks()).Returns(bookList);

            PriceProcessor priceProcessor = new PriceProcessor(bookRepoMock.Object);

            Assert.Equal(60000, priceProcessor.GetTotalPrice());

        }
    }
}
